require 'socket'

request = "GET /websiteos/example_of_a_simple_html_page.htm HTTP/1.1"
hostheader = "host: www.help.websiteos.com"
socket = TCPSocket.open('www.help.websiteos.com', 80)
socket.puts(request)
socket.puts(hostheader)
socket.puts("\n")
response = socket.read
href_count = response.scan('href').count
puts "No of href's #{href_count}"
