require 'socket'

host = 'www.help.websiteos.com'
port = 80
path = '/websiteos/example_of_a_simple_html_page.htm'
request = "GET #{path} HTTP/1.1"
hostheader = "host: #{host}"
socket = TCPSocket.open(host, port)
socket.puts(request)
socket.puts(hostheader)
socket.puts ""
response = socket.read
number_of_links = response.scan('href').count
puts number_of_links