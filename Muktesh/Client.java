import java.io.*;
import java.net.*;

public class Client {
    public static void main(String[] args) throws Exception {
        String url = "https://en.wikipedia.org/wiki/Main_Page";
        System.out.println(url);
        getHTML(url);
    }

    public static void getHTML(String givenURL) throws Exception {
        try {
            URL url = new URL(givenURL);
            URLConnection urlcon = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));
            System.out.println("protocol: " + url.getProtocol());
            System.out.println("host: " + url.getHost());
            System.out.println("port: " + url.getPort());
            System.out.println("file: " + url.getFile());
            int i=0;
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                if (line.contains("href=\"") || line.contains("href=\'")) {
                    System.out.println(line.trim());
                    i++;
                }
            }
            System.out.println(i);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}