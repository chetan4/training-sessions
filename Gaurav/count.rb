require 'socket'

class Count
  attr_reader :host, :port, :path
  def initialize(host = 'help.websiteos.com', port = 80, path = "/websiteos/example_of_a_simple_html_page.htm")
    @host = host
    @port = port
    @path = path
  end

  def count_tag(tag)
    data = ''
    Socket.tcp(host, 80) { |socket|
      socket.print "GET #{path} HTTP/1.0\r\nHost: #{host}\r\n\r\n\r\n"
      data = socket.read
    }
    data.scan('href').count
  end
end

key = 'href'
counter = Count.new
response = counter.count_tag(key)
puts "----------------------------------------------------------"
puts ("\n\ntotal '#{key}' count : " + response.to_s + "\n\n").upcase
puts "----------------------------------------------------------"
