#Program to call a web page and count number of href tags using socket
import socket
import re
from BeautifulSoup import BeautifulSoup

finalHTML=""
hrefCountUsingLibrary = 0
hrefCountUsingRegex = 0

socketObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socketObject.connect(("help.websiteos.com", 80))
requestHeader = "GET /websiteos/example_of_a_simple_html_page.htm HTTP/1.1\nHost: help.websiteos.com\n\n"
socketObject.send(requestHeader)
htmlResponseChunk=socketObject.recv(4096)

while(len(htmlResponseChunk)>0):
    finalHTML=finalHTML+htmlResponseChunk
    htmlResponseChunk = socketObject.recv(4096)
socketObject.close()

#Finding href from <a> tag using BeautifulSoup
soup = BeautifulSoup(finalHTML)
for tag in soup.findAll('a', href=True):
    hrefCountUsingLibrary = hrefCountUsingLibrary + 1

#Finding the href using regex
allHrefs = re.findall(r'href=[\'"]?([^\'" >]+)', finalHTML)
for href in allHrefs:
    hrefCountUsingRegex = hrefCountUsingRegex + 1
print "{}:{}".format("href tag count using Library(BeautifulSoup)",hrefCountUsingLibrary)
print "{}:{}".format("href count using regex",hrefCountUsingRegex)
